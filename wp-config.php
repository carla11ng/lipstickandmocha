<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lipstick_and_mocha');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=$.9^VRm>:-zOZr/O$p^_{vT_B{&c_(.)Bf9kt?CeO_0TuLBv7Z6spT)vtlM26dh');
define('SECURE_AUTH_KEY',  'n%S@rNqa~EVv;fiCFXQ:>49xQZm=<M6j=-Q<+Z[ _ prlF-`OR|wVE$5t?[J}~<u');
define('LOGGED_IN_KEY',    'BvIi<}O^!5$Q)T`v6A>,os$NoeuYG9_,Z/z?_S|MkxBn&00 qm&]tFEE)EyGZuT^');
define('NONCE_KEY',        'c);jm6c3E.a.FTmYyUjVj3O@:k;PU|`PRteB`eJx1p;C`1FZIcMoxP_ FlwPsFmU');
define('AUTH_SALT',        'MQphMzLA{:j5jCuCoQnA@ztrWj&fr^0._J<Clin0K6T2D?[dpA9G}>$P8J>+8x]Y');
define('SECURE_AUTH_SALT', '=A:wXEmJ1SLGOd&HT*|ww.%&-db.5~%G=-#=7Qi9U/+ZAvo9KT^3IzS;|mdA9=~F');
define('LOGGED_IN_SALT',   '#44BO<{Ux:/7FRDQUQx;pGXI)JJ4x(Zy>)iO8bpu|_>L1S9,LCxQ,0[(-{TK#wzp');
define('NONCE_SALT',       '3]P5gC(Y!e^_zqZnG~wQgq@#z^eOZa2R%f>T<x@+-R^dz5!gM@g*h.mL?Z,_qJT-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
