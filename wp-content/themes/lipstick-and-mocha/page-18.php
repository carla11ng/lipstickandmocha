<?php
/**
 * The template for displaying a specific page: ARCHIVE.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lipstick-and-Mocha
 */

get_header(); ?>

	<div id="page">

		<?php 
			$recent = new WP_Query("page_id=18");
			while($recent->have_posts()) : $recent->the_post();
		    	the_content();
			endwhile;
		?>

	</div>

<?php get_footer(); ?>
