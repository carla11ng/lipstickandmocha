


									/*******************************************
									 *										   *
									 *            $(document).ready	           *
									 *										   *
									 *******************************************/

$(document).ready(function(){


	// Responsive menu + Hamburger icon animation
	$("#nav-icon").click(function(){
		$(this).toggleClass("open");

		if (!$("header .main-menu ul").is(":visible"))
	        $('header .main-menu ul').addClass("open");

	    $("header .main-menu ul").slideToggle(function() { 
	        if (!$("header .main-menu ul").is(":visible"))
	            $('header .main-menu ul').removeClass("open");
	    });
	});



});
