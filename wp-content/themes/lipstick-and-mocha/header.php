<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="site-content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Lipstick-and-Mocha
 */

?>
<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.png"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/style.css"/>
	</head>

	<body>
		<div id="wrapper">
	 
			<header>
				<!-- Logo-->
				<div id="logo-container">
					<a href="<?php bloginfo('url'); ?>"></a>
				</div>

				<!-- Menu -->
				<div class="main-menu">
					<div id="burger-nav">
						<div id="nav-icon">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>

					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'main-menu') ); ?>

				</div>
			</header>


			<div id="site-content">
