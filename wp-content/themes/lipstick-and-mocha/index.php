<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lipstick-and-Mocha
 */

get_header(); ?>


<div id="posts-all" class="container-fluid" style="text-align: center;"> 

		<?php
		wp_reset_query();
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		$args = array(
			'posts_per_page' => 9,
			'paged' => $paged
		);
		query_posts($args);

		if ( have_posts() ) :
			while ( have_posts() ) : the_post();	
		?>
				<a class="post-container" href="<?php echo get_permalink() ?>" style="background: url(<?php echo catch_that_image();?>) no-repeat; background-size: 100% auto; background-position: center;">
					<div class="post-white-background">
						<div class="post-info">
							<div class="post-date"><?php the_time('d/m/Y'); ?></div>
							<div class="post-title"><?php the_title(); ?></div>
							<?php 
								$category = get_the_category();
								$firstCategory = $category[0]->cat_name;
							?>
							<div class="post-category"><?php echo strtoupper($firstCategory); ?></div>
						</div>
					</div>
				</a>
		<?php endwhile; endif; ?>


</div> <!-- #posts-all -->


<div class="container-fluid">
	<div id="goodies" class="row">

		<!-- Goodreads -->
		<div class="goodies-box col-xs-12 col-sm-6 col-md-4" id="goodreads-favorites">
			<div>GOODREADS FAVORITES</div>
			Goodreads Faves
		</div>

		<!-- Netgalley Badges-->
		<div class="goodies-box col-xs-12 col-sm-6 col-md-4" id="netgalley-badges">
			<div>NETGALLEY BADGES</div>
			<img src="https://s2.netgalley.com/badge/34ce7850dcc0c28b64ea24418fc0311982ee282a" width="80" height="80" alt="Professional Reader" title="Professional Reader"/>
			<img src="https://s2.netgalley.com/badge/2ae8ab66554d55abeebedc480804bb8b11ba5af7" width="80" height="80" alt="2015 Challenge Participant" title="2015 Challenge Participant"/>
			<img src="https://s2.netgalley.com/badge/3f5eefa7a6fa260e829a85edca3990a3fde2dd7d" width="80" height="80" alt="Reviews Published" title="Reviews Published"/>
			<img src="https://s2.netgalley.com/badge/95d2a381ccfaeb3abb331a5ca5de6df8b97d1e60" width="80" height="80" alt="80%" title="80%"/>
			<img src="https://s2.netgalley.com/badge/f2a17032d4a28cc40246df02ae7b1b8dce295b23" width="80" height="80" alt="2016 NetGalley Challenge" title="2016 NetGalley Challenge"/>
			<img src="https://s2.netgalley.com/badge/270f6f32f4e63e76f91a645ccf3b35576e70cab9" width="80" height="80" alt="50 Book Reviews" title="50 Book Reviews"/>
		</div>

		<!-- Follow Me -->
		<div class="goodies-box col-xs-12 col-sm-6 col-md-4" id="follow-me">
			<div>FOLLOW ME</div>
			<a href="https://www.bloglovin.com/blogs/lipstick-mocha-11310257" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/follow_bloglovin.png" alt="Bloglovin" title="Follow me on Bloglovin"/></a>
			<a href="https://www.goodreads.com/user/show/29274043-carla" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/follow_goodreads.png" alt="Goodreads" title="Follow me on Goodreads"/></a>
			<a href="https://www.instagram.com/carla11ng/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/follow_instagram.png" alt="Instagram" title="Follow me on Instagram"/></a>
			<a href="https://www.pinterest.com/carla11ng/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/follow_pinterest.png" alt="Pinterest" title="Follow me on Pinterest"/></a>
			<a href="https://twitter.com/carla_ng" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/follow_twitter.png" alt="Twitter" title="Follow me on Twitter"/></a>
			<a href="http://carla-ng.deviantart.com/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/follow_deviantart.png" alt="Deviantart" title="Follow me on Deviantart"/></a>
		</div>

	</div>
</div>

<?php get_footer(); ?>
