<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Lipstick-and-Mocha
 */

?>

			</div> <!-- #site-content-->


			<footer class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-sm-offset-2">
						&#169; COPYRIGHT 2013-2016 CARLA N. 
					</div>
					<div class="col-xs-12 col-sm-5 col-sm-pull-1">
						BLOG DESIGNED AND DEVELOPED BY CARLA N.
					</div>
				</div>
			</footer>

		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> <!-- jQuery -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> <!-- Bootstrap -->
	    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script>

    	</div> <!-- #wrapper -->
	</body>
	
</html>
